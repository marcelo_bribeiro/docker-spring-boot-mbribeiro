FROM openjdk:12
ADD target/docker-spring-boot-mbribeiro.jar docker-spring-boot-mbribeiro.jar
EXPOSE 8085
ENTRYPOINT ["java", "-jar", "docker-spring-boot-mbribeiro.jar"]

